# VueFormInputs

Basic vue.js form inputs, easily customisable.
Using bootstrap classes

## Requires:
```
"bootstrap": "^4.1.0",
"vue": "^2.5.17",
"@fortawesome/fontawesome-free": "^5.8.1",
"vue-bootstrap-datetimepicker": "^5.0.1",
```